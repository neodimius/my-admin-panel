<?php
    class Menu{
        static public function createMenu(){
            $sql = "SELECT * FROM `Pages` WHERE `inMenu` = ? ORDER BY `orderMenu`";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, 1);
            $stmt ->execute();
            return $stmt -> fetchAll(PDO::FETCH_ASSOC);
        }
    }

?>