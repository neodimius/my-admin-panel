<?php
    class Users{
        public function addUser($name, $lastName, $age, $phone, $login, $password, $ava){
            $rand =md5(random_int(20, 10000));

            $sql = "INSERT INTO `Users` (`name`, `lastName`, `age`, `phone`, `login`, `password`, `ava`, `tok`) VALUES (?,?,?,?,?,?,?,?)";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $name);
            $stmt -> bindValue(2, $lastName);
            $stmt -> bindValue(3, $age);
            $stmt -> bindValue(4, $phone);
            $stmt -> bindValue(5, $login);
            $stmt -> bindValue(6, $password);
            $stmt -> bindValue(7, $ava);
            $stmt -> bindValue(8, $rand);
            $stmt -> execute();
            //return Db::connect() ->lastInsertId();
            
            
        }

        public function updateUser($id, $name, $lastName, $age, $phone, $login, $password, $ava){
            $sql = "UPDATE `Users` SET `name` = ?, `lastName`=?, `age`=?, `phone`=?, `login`=?, `password`=?, `ava`=? WHERE `id` = '$id'";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $name);
            $stmt -> bindValue(2, $lastName);
            $stmt -> bindValue(3, $age);
            $stmt -> bindValue(4, $phone);
            $stmt -> bindValue(5, $login);
            $stmt -> bindValue(6, $password);
            $stmt -> bindValue(7, $ava);
            $stmt -> execute();
            
        }

        public function getUsers (){
            $res = Db::connect() -> query("SELECT * FROM `Users`");
            return $res -> fetchAll(PDO::FETCH_ASSOC);
        }

        public function getUser($id){
            $res = Db::connect() -> query("SELECT * FROM `Users` WHERE `id` = '$id'");
            return $res -> fetch(PDO::FETCH_ASSOC);
        }

        public function deleteUser($id){
            $res = Db::connect() -> query("DELETE FROM `Users` WHERE `Users`.`id` = '$id'");
            return $res -> fetch(PDO::FETCH_ASSOC);
        }
    }
?>