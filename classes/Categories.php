<?php 
    class Categories{
        public function addCategories($cat){
            $sql = "INSERT INTO `Category` (`catName`) VALUES (?)";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $cat);
            $stmt -> execute();
        }

        public function getCategories(){
            $res = Db::connect() -> query("SELECT * FROM `Category`");
            return $res ->fetchAll(PDO::FETCH_ASSOC);
        }

        public function deleteCategories($id){
            $sql = "DELETE FROM `Category` WHERE `id` =?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $id);
            $stmt -> execute();
        }
    }
?>