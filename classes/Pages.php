<?php
    class Pages{
        public function addPage($title, $content, $img, $link, $postType, $inMenu, $orderMenu){
            $sql = "INSERT INTO `Pages` (`title`, `content`, `img`, `link`, `postType` ,`inMenu`, `orderMenu`) VALUES(?, ?, ?, ?, ?, ?, ?)";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $title);
            $stmt -> bindValue(2, $content);
            $stmt -> bindValue(3, $img);
            $stmt -> bindValue(4, $link);
            $stmt -> bindValue(5, $postType);
            $stmt -> bindValue(6, $inMenu);
            $stmt -> bindValue(7, $orderMenu);

            $stmt -> execute();
        }

        public function updatePage($id, $title, $content, $img, $link){
            $sql = "UPDATE `Pages` SET `title`=?, `content`=?, `img`=?, `link`=? WHERE `id` = $id";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $title);
            $stmt -> bindValue(2, $content);
            $stmt -> bindValue(3, $img);
            $stmt -> bindValue(4, $link);
            $stmt -> execute();
        }

        public function getPages($postType){
            $sql = "SELECT * FROM `Pages` WHERE `postType` = ?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $postType);
            $stmt ->execute();
            return $stmt -> fetchAll(PDO::FETCH_ASSOC);
        }

        public function getPage($id){
            $sql = "SELECT * FROM `Pages` WHERE `id` = ?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $id);
            $stmt ->execute();
            return $stmt -> fetch(PDO::FETCH_ASSOC);

        }

        public function getSitePage($link){
            $sql = "SELECT * FROM `Pages` WHERE `link` = ?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $link);
            $stmt ->execute();
            return $stmt -> fetch(PDO::FETCH_ASSOC);
        }

        public function deletePage($id){
            $sql = "DELETE FROM `Pages` WHERE `id` =?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $id);
            $stmt -> execute();
            return $id;
        }
    }
    
?>