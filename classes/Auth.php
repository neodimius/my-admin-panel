<?php
    class Auth{
        protected $maxSessionTime = 2; // Max Session time to authorization in the minutes;

        public function authUser($login, $pass){
// Connect to DataBase and get User if data`s true
            $sql = "SELECT * FROM `Users` WHERE `login` = ? AND `password` = ?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $login);
            $stmt -> bindValue(2, $pass);
            $stmt ->execute();
            $res = $stmt -> fetchAll(PDO::FETCH_ASSOC);

            if ($res){
               $tok = md5(random_int(20, 10000));   //Generate token
               $tokTime = time() + $this -> maxSessionTime * 60; 
               $id = $res[0]['id'];

                $sql2 = "UPDATE `Users` SET `tok`= ?, `tokTime` = ? WHERE `id` = $id";
                $stmt = Db::connect() -> prepare($sql2);
                $stmt -> bindValue(1, $tok);
                $stmt -> bindValue(2, $tokTime);
                $stmt -> execute();
                $_SESSION['tok'] = $tok;
                $_SESSION['name'] = $res[0]['name'];
            }
            else return "Wrong User";
        }

        public function checkUser($name, $tok){
            $sql = "SELECT * FROM `Users` WHERE `tok` = ? AND `name` = ?";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, $tok);
            $stmt -> bindValue(2, $name);
            $stmt ->execute();
            $res = $stmt -> fetchAll(PDO::FETCH_ASSOC);
            $currentTime =time();
            if ($currentTime < $res[0]['tokTime'] && $res[0]['tokTime'] !=0)
                return $res[0]['name'];
            else return false;
        }

        public function exitUser($tok){
            $sql = "UPDATE `Users` SET `tok`= ?, `tokTime` = ? WHERE `tok` = $tok";
            $stmt = Db::connect() -> prepare($sql);
            $stmt -> bindValue(1, 0);
            $stmt -> bindValue(2, 0);
            $stmt -> execute();
            session_destroy();
        }
    }
?>