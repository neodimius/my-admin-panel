<h2>All Posts</h2>
<div class="col-md-3 bg-dark">
    <div><a href="adminPanel.php?page=AddPost">AddPost</a></div>
    
</div>
<table>
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Link</th>
        <th>Preview</th>
        <th>Login</th>
        <th></th>
        <th></th>
    </tr>
 
<?php
    $pages = new Pages;
    $n=0;
    foreach($pages -> getPages("Post") as $value):?>
   
   <tr>
       <td><?php echo ++$n;?></td>
       <td><?php echo $value['title'];?></td>
       <td><?php echo $value['link'];?></td>
       <td><img class="ava" src="./images/<?php echo $value['img'];?>"></td>
       <td><a href="./adminPanel.php?page=EditPost&id=<?php echo $value['id'];?>">Edit</a></td>
       <td><a href="./model/DeletePage.php?id=<?php echo $value['id'];?>">Delete</a></td>
   </tr>
<?php endforeach; ?>
</table>