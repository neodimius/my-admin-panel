
<script src="https://cdn.ckeditor.com/ckeditor5/10.1.0/classic/ckeditor.js"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <h1>Add new Post</h1>
            <form method="post" enctype="Multipart/form-data" action="./model/AddPage.php">
                <div><span>Title:</span><textarea name="title" id="" cols="30"></textarea></div>
                <div><span>Content:</span><textarea name="content" id="editor"></textarea></div>
                <div><span>Link:</span><input type="text" name="link"></div>
                <div><span>Order menu:</span><input type="text" name="orderMenu"></div>
                <div><span>In menu:</span>Yes<input type="radio" name="inMenu" value="1">No<input type="radio" name="inMenu" value="0" checked></div>
                <input type="hidden" name="postType" value="Post">
                <div><span>Load image:</span><input type="file" name="ava"></div>
                <div><input type="submit"></div>
            </form>
        </div>
    </div>
</div>
<script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>