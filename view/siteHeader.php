<?php
// Auth
    session_start();
    include "autoload.php";
    $page = new Pages;
    $clean = new Nav;
    $auth = new Auth;
    include "model/checkUser.php";

    if (empty($_GET['page'])){
        $siteContent = $page -> getSitePage('main');
    } else{
        $siteContent = $page -> getSitePage($clean->clean($_GET['page']));
    }
    
    
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <title><?php echo $content['title'];?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
       <header>
            <div class="container-fluid">
                <div class="row d-flex justify-content-between">
                    <div>
                        <h2>My site</h2>
                    </div>
                    <div>
                        <?php if ($userName) {
                                echo 'Hi ' . $userName;
                                echo "<a href='index.php?out=true' class='btn text-light bg-secondary'>Exit</a>";
                            }  else include "view/Auth.php";
                            ?>
                    </div>
                    <div>
                        <?php if ($userName) echo "<a href='adminPanel.php' class='btn text-light bg-secondary'>Admin Panel</a>";?>
                    </div>
                </div>
            </div>
        </header>
