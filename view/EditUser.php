<?php 

    $user = new Users;
    $value = $user-> getUser($_GET['id']);
    ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <form method="post" enctype="Multipart/form-data" action="./model/EditUser.php">
                <div><span>Name:</span><input type="text" name="name" value="<?php echo $value['name'];?>"></div>
                <div><span>Last Name:</span><input type="text" name="lastName" value="<?php echo $value['lastName'];?>"></div>
                <div><span>Age:</span><input type="text" name="age" value="<?php echo $value['age'];?>"></div>
                <div><span>Phone:</span><input type="text" name="phone" value="<?php echo $value['phone'];?>"></div>
                <div><span>Login:</span><input type="text" name="login" value="<?php echo $value['login'];?>"></div>
                <div><span>Password:</span><input type="password" name="password"></div>
                <input type="text" style="display:none" name ="ava0" value="<?php echo $value['ava'];?>">
                <input type="text" style="display:none" name="id" value="<?php echo $_GET['id'];?>">
                <div><span>Change avatar:</span><input type="file" name="ava"></div>
                <div><input type="submit"></div>
            </form>
        </div>
        <div class="col-md-5">
            <img class="ava" src="<?php echo $value['ava'];?>" alt="">
        </div>
    </div>
</div>