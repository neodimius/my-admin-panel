<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin OOP</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
       <header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Adminka</h2>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <?php if ($userName) {
                                echo 'Hi ' . $userName;
                                echo "<a href='index.php?out=true' class='btn text-light bg-secondary'>Exit</a>";
                            }  else include "view/Auth.php";
                            ?>
                    </div>
                    <div class="col-md-2">
                        <a href='index.php' class='btn text-light bg-secondary'>Back to website</a>
                    </div>
                    </div>
                </div>
            </div>
        </header>
