<h2>All Users</h2>
<div class="col-md-3 bg-dark">
    <div><a href="adminPanel.php?page=AddUser">AdUser</a></div>
    
</div>
<table>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Phone</th>
        <th>Login</th>
        <th>Avatar</th>
    </tr>
 
<?php
    $user = new Users;
    $n=0;
    foreach($user -> getUsers() as $value):?>
   
   <tr>
       <td><?php echo ++$n;?></td>
       <td><?php echo $value['name'];?></td>
       <td><?php echo $value['lastName'];?></td>
       <td><?php echo $value['age'];?></td>
       <td><?php echo $value['phone'];?></td>
       <td><?php echo $value['login'];?></td>
       <td><img class="ava" src="<?php echo $value['ava'];?>"></td>
       <td><a href="./adminPanel.php?page=EditUser&id=<?php echo $value['id'];?>">Edit</a></td>
       <td><a href="./model/DeleteUser.php?id=<?php echo $value['id'];?>">Delete</a></td>
   </tr>
<?php endforeach; ?>
</table>