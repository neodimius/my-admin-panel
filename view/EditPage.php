<?php 

    $page = new Pages;
    
    $value = $page -> getPage($_GET['id']);
    
    ?>
<script src="https://cdn.ckeditor.com/ckeditor5/10.1.0/classic/ckeditor.js"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <h1>Edit Page</h1>
            <form method="post" enctype="Multipart/form-data" action="./model/EditPage.php">
                <div><span>Title:</span><textarea name="title" cols="30"><?php echo $value['title'];?></textarea></div>
                <div><span>Content:</span><textarea name="content" id="editor"><?php echo $value['content'];?></textarea></div>
                <div><span>Link:</span><input type="text" name="link" value="<?php echo $value['link'];?>"></div>
                <div><span>Order menu:</span><input type="text" name="orderMenu" value="<?php echo $value['orderMenu'];?>"></div> 
                <div><span>Load avatar:</span><input type="file" name="ava"></div>
                <input type="text" style="display:none" name ="ava0" value="<?php echo $value['ava'];?>">
                <input type="text" style="display:none" name ="destination" value="Pages">
                <input type="text" style="display:none" name ="id" value="<?php echo $value['id'];?>">
                <div><input type="submit"></div>
            </form>
        </div>

        <div class="col-md-5">
            <img class="ava" src="./images/<?php echo $value['img'];?>" alt="">
        </div>
    </div>
</div>
<script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>