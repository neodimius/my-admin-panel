<?php 
    session_start();
    
    include "autoload.php";
    $auth = new Auth;
    $clean = new Nav;
    if (!empty($_POST['login']) && !empty($_POST['pass'])){
        $res = $auth -> authUser($clean->clean($_POST['login']), $clean->clean($_POST['pass']));
        if ($res) $_SESSION['authError'] = "Login or password is wrong";
    }
    Header("Location: ../index.php");
?>