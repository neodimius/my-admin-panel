<?php
    include "autoload.php";
    $page = new Pages;
    $nav = new Nav;

    if (!empty($_FILES['ava'])){
        $upload = new UploadImage;
        $img = "pages/" . $_POST['link'];
        $upload -> upload($_FILES['ava'], $img);
    } else $img = $_POST['ava0'];

    $page -> updatePage($nav->clean($_POST['id']),$nav->clean($_POST['title']), $_POST['content'], $img, $nav->clean($_POST['link']));

    Header("Location: ../adminPanel.php?page=". $_POST['destination']);
?>